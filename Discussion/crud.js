let http = require("http");

// Mock database 

let directory = [
    {
        "name": "Rendon",
        "email": "rendon@gmail.com"
    },
    {
        "name": "Jobert",
        "email": "jobert@gmail.com"
    },
    {
        "name": "CongTv",
        "email": "congtv@gmail.com"
    },
    {
        "name": "CongTv",
        "email": "congtv@gmail.com"
    },
    {
        "name": "Pingris",
        "email": "pingris@gmail.com"
    }
];

http.createServer((request, response) => {

    if (request.url == "/users" && request.method == "GET") {

        response.writeHead(200, { "Content-Type": "application/json" });
        // JSON.stringify() - Transforms javascript into JSON string
        response.write(JSON.stringify(directory));
        response.end();
    }
    // Route for creating new item upon receiving a POST request
    // A request object contains several parts:
    //== Headers - contains information about the request
    // ==== context/content like what is the data type.
    //== Body - contains the actual information being sent with the request.
    // ==== Note: The request body can NOT be empty. 

    else if (request.url == "/users" && request.method == "POST") {

        // Declare and initialize a "requestBody" variable to an empty string
        // This will act as a placeholder for the resource/data to be created later on.


        let requestBody = "";

        // A stream is sequence of data
        // Data is receive from the client and is processed in the 'data' stream
        // The information provided from the request object enters a sequence called "data" the code below will be triggered
        // data step - this reads the "data" stream and process it as the request body.

        // on method binds an even to an object
        // It is a way to express your intent if there is something happening (data sent or error in your case), then it executes the function added as a parameter. This style of programming is called even-driven programming.
        // event-driven-programming is a programming paradigm in which the flow of the program is determined by even such as user action (mouse clicks, key presses or message passing from other programs or threads)

        // Paulo - CEO
        // Charicez - Crew

        request.on('data', (data) => {
            requestBody += data
        });

        request.on('end', () => {

            // Check if at this point the requestBody is of data type STRING
            // We need this to be of data type JSON to access this

            console.log(typeof requestBody)

            // JSON.parse() - Takes a JSON string and transform it into javascript objects.

            requestBody = JSON.parse(requestBody);

            // Creates a new object representing the new mock database record 

            let newUser = {
                "name": requestBody.name,
                "email": requestBody.email
            }

            // Add the user into mock database
            directory.push(newUser);
            console.log(directory);
            response.writeHead(200, { 'Content-Type': 'application/json' });
            response.end();
        });

    }
}).listen(4000)

console.log("Running in localhost:4000")